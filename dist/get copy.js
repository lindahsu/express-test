"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var router = _express["default"].Router();

router.get("/", function (req, res) {
  res.send("Hello World!");
});
router.get("/yeojin", function (req, res) {
  res.send("yeojin");
});
router.get("/hyesoo", function (req, res) {
  res.send("hyesoo");
});
router.get("/test", function (req, res) {
  console.log(req.query);

  if (parseInt(req.query.id) === 1) {
    res.send("첫번째 게시글");
  }

  if (parseInt(req.query.id) === 2) {
    res.send("두번재 게시글");
  }
});
var _default = router;
exports["default"] = _default;