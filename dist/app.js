"use strict";

var _express = _interopRequireDefault(require("express"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _get = _interopRequireDefault(require("./get"));

var _post = _interopRequireDefault(require("./post"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = (0, _express["default"])();

var cors = require("cors");

app.use(_express["default"].json());
app.use(cors()); // app.use(bodyParser.urlencoded({ extended: true }));

app.use("/get", _get["default"]); // any get method

app.use("/post", _post["default"]); // any post method and upload

app.listen(4000, function () {
  console.log("server started");
});