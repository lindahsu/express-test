import express from "express";
import bodyParser from "body-parser";
import get from "./get";
import post from "./post";
const app = express();
const cors = require("cors");
app.use(express.json());
app.use(cors());
// app.use(bodyParser.urlencoded({ extended: true }));

app.use("/get", get); // any get method
app.use("/post", post); // any post method and upload

app.listen(4000, () => {
  console.log("server started");
});
