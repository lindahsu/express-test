import express from "express";
const router = express.Router();

router.get("/", (req, res) => {
  res.send("200");
});

router.get("/params/:test/:token", (req, res) => {
  console.log(req.params);
  res.send("200");
});

router.get("/query", (req, res) => {
  console.log(req.query);
  res.send("200");
});

router.get("/vodreplay", (req, res) => {
  // console.log(req.params.id);
  console.log(req.query.id);
  // if (req.query.id !== "1") res.json(false);

  try {
    // uid === 1
    const data = [
      {
        user: {
          dueData: 80,
          lecture: {
            lecture_title: "강의",
            writer: "작성자",
            viewCount: 3,
            createdAt: "2020-00-00",
            vod_url: "http://어쩌구",
            vod_detail: "컨텐츠 상세내용",
          },
        },
      },
      {
        user: {
          dueData: 80,
          lecture: {
            lecture_title: "강의2",
            writer: "작성자",
            viewCount: 3,
            createdAt: "2020-00-00",
            vod_url: "http://어쩌구",
            vod_detail: "컨텐츠 상세내용",
          },
        },
      },
    ];

    // console.log(data[0]);
    //res.json(true);
    res.json(data[parseInt(req.query.id)]);
  } catch (error) {
    res.json(false);
  }
});

router.get("/replay", (req, res) => {
  try {
    const reuslt = [
      {
        uid: 1,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 2,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
        imageUrl: "https://naver.com/eaefa.jpg",
      },
      {
        uid: 3,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 4,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 5,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 6,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 7,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 8,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 9,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
      {
        uid: 10,
        title: "Admin > VOD 컨텐츠 등록 시 입력된 제목이 표기되는 영역입니다.",
        nth: 44,
        viewConunt: 124731,
        createAt: "2021.00.00",
      },
    ];
    res.json(reuslt);
  } catch (error) {
    res.json(false);
  }
});

// router.get("/yeojin", (req, res) => {
//   res.send("yeojin");
// });

// router.get("/hyesoo", (req, res) => {
//   res.send("hyesoo");
// });

// router.get("/test", (req, res) => {
//   console.log(req.query);
//   if (parseInt(req.query.id) === 1) {
//     res.send("첫번째 게시글");
//   }

//   if (parseInt(req.query.id) === 2) {
//     res.send("두번재 게시글");
//   }
// });

export default router;
